<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategorizeablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categorizeables', function (Blueprint $table) {
        	$table->foreignId("category_id");
        	$table->integer("categorizeable_id");
        	$table->char("categorizeable_type" , 100);

        	$table->foreign("category_id")->references("id")->on("categories")->cascadeOnDelete();
        	$table->primary(["category_id" , "categorizeable_id" , "categorizeable_type"],"categorizeables_primaries");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categorizables');
    }
}
