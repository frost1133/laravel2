<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilePackageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('file_package', function (Blueprint $table) {
            $table->foreignId("file_id");
            $table->foreignId("package_id");
            $table->timestamps();

            $table->primary(['file_id' , 'package_id']);
            $table->foreign("file_id")->references("id")->on("files")->cascadeOnDelete();
            $table->foreign("package_id")->references("id")->on("packages")->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('file_package');
    }
}
