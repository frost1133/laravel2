<!doctype html>
<html lang="fa">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="/css/app.css">
	<title>{{env("APP_NAME")}}</title>
</head>
<body>
<div id="app">
	<router-view></router-view>
</div>
<script src="/js/app.js" type="text/javascript"></script>
</body>
</html>
