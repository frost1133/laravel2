<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/bootstrap-rtl.min.css">
    <link rel="stylesheet" href="/css/select2.min.css">
    @yield("pageStyle")
    <title>{{env("APP_NAME")}}</title>
</head>
<body>
@include("Admin.Partials.Nav")

<div class="container">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            @yield("content")
        </div>
    </div>
</div>
<!-- Optional JavaScript -->

<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="/js/jquery-3.5.1.min.js" type="text/javascript"></script>
<script src="/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/js/select2.min.js" type="text/javascript"></script>

@yield("pageScript")


</body>
</html>
