<!doctype html>
<html lang="fa">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/bootstrap-rtl.min.css">

    <link rel="stylesheet" href="/css/select2.min.css">

    @yield("pageStyle")
    <title>{{env("APP_NAME")}} </title>
</head>
<body>
@include("App.Partials.Nav")

<div class="container">
    <div class="row">
        @yield("content")
    </div>
</div>
<!-- Optional JavaScript -->

<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="/js/jquery-3.5.1.min.js" type="text/javascript"></script>
<script src="/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/js/select2.min.js" type="text/javascript"></script>

@yield("pageScript")
</body>
</html>
