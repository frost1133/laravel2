<tr>
    <td>{{$plan->id}}</td>
    <td>{{$plan->name}}</td>
    <td>{{$plan->limit_download_count}}</td>
    <td>{{$plan->price}}</td>
    <td>{{$plan->days_count}}</td>
    <td style="text-align: center">
        <form action="{{route("admin.plans.delete" , $plan)}}" method="post">
            @csrf
            @method("DELETE")
            <div class="btn-group">
                <a href="{{route("admin.plans.edit" , $plan)}}" class="btn btn-success"><i class="glyphicon glyphicon-edit"></i></a>
                <button type="submit" class="btn btn-danger"><i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </form>
    </td>
</tr>
