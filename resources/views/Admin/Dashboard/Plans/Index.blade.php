@extends("Layouts.Admin")
@section("content")
    <div class="panel panel-default">
        <div class="panel-heading">لیست طرح ها</div>
        <div class="panel-body">
            @include("Admin.Partials.Notifications")
            <table class="table table-hover">
                <thead>
                    <tr>
                        <td>شناسه</td>
                        <td>عنوان</td>
                        <td>محدودیت دانلود</td>
                        <td>قیمت</td>
                        <td>تعداد روز اعتبار</td>
                        <td>عملیات</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($plans as $plan)
                        @include("Admin.Dashboard.Plans.PlanItem" , $plan)
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    </div>
@endsection
