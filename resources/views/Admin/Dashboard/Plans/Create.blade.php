@extends("Layouts.Admin")
@section("content")
    <div class="panel panel-default">
        <div class="panel-heading">ثبت کاربر جدید</div>
        <div class="panel-body">
            <div class="row">
                <form class="col-sx-12 col-md-6" action="{{route("admin.plans.store")}}" method="post">
                    @csrf
                    @include("Admin.Partials.Errors")
                    <div class="form-group">
                        <lable for="name">عنوان طرح:</lable>
                        <input type="text" name="name" id="name" placeholder="عنوان طرح..." class="form-control" value="{{old("name")}}">
                    </div>
                    <div class="form-group">
                        <lable for="limitD">محدودیت دانلود روزانه:</lable>
                        <input type="number" name="limitD" id="limitD" placeholder="محدودیت دانلود روزانه..." class="form-control" value="{{old("limitD")}}">
                    </div>
                    <div class="form-group">
                        <lable for="price">قیمت:</lable>
                        <input type="number" name="price" id="price" placeholder="قیمت..." class="form-control" value="{{old("price")}}">
                    </div>
                    <div class="form-group">
                        <lable for="days">تعداد روز اعتبار::</lable>
                        <input type="number" name="days" id="days"placeholder="تعداد روز اعتبار..." class="form-control" value="{{old("days")}}">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-success" type="submit">ثبت</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>
@endsection
