
@extends("Layouts.Admin")

@section("content")
    <?php
        global $PlanData;
        $PlanData = $plan;
    function planData ($filed){
        global $PlanData;
        if ( old($filed) == null){
            switch ($filed){
                case "limitD" :
                    $filed = "limit_download_count";
                    break;
                case "days" :
                    $filed = "days_count";
                    break;
            }
            return $PlanData->$filed;
        }
        return old($filed);
    }
    ?>
    <div class="panel panel-default">
        <div class="panel-heading">ویرایش طرح</div>
        <div class="panel-body">
            <div class="row">
                <form class="col-sx-12 col-md-6"   action="{{route("admin.plans.update" , $plan)}}" method="post">
                    @csrf
                    @method("PATCH")
                    @include("Admin.Partials.Errors")
                    <div class="form-group">
                        <lable for="name">عنوان طرح:</lable>
                        <input type="text" name="name" id="name" placeholder="عنوان طرح..." class="form-control" value="{{planData("name")}}">
                    </div>
                    <div class="form-group">
                        <lable for="limitD">محدودیت دانلود روزانه:</lable>
                        <input type="number" name="limitD" id="limitD" placeholder="محدودیت دانلود روزانه..." class="form-control" value="{{planData("limitD")}}">
                    </div>
                    <div class="form-group">
                        <lable for="price">قیمت:</lable>
                        <input type="number" name="price" id="price" placeholder="قیمت..." class="form-control" value="{{planData("price")}}">
                    </div>
                    <div class="form-group">
                        <lable for="days">تعداد روز اعتبار::</lable>
                        <input type="number" name="days" id="days"placeholder="تعداد روز اعتبار..." class="form-control" value="{{planData("days")}}">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-success" type="submit">ثبت</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>
@endsection


