<tr>
    <td>{{$package->id}}</td>
    <td>{{$package->title}}</td>
    <td>{{$package->price}}</td>
    <td style="text-align: center">
        <form action="{{route("admin.packages.delete" , $package)}}" method="post">
            @csrf
            @method("DELETE")
            <div class="btn-group">
                <a href="{{route("admin.packages.sync_file" , $package)}}" class="btn btn-info"><i class="glyphicon glyphicon-file"></i></a>
                <a href="{{route("admin.packages.edit" , $package)}}" class="btn btn-success"><i class="glyphicon glyphicon-edit"></i></a>
                <button type="submit" class="btn btn-danger"><i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </form>
    </td>
</tr>
