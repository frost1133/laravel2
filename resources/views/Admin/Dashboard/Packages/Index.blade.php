@extends("Layouts.Admin")
@section("content")
    <div class="panel panel-default">
        <div class="panel-heading">لیست پکیج ها</div>
        <div class="panel-body">
            @include("Admin.Partials.Notifications")
            <table class="table table-hover">
                <thead>
                    <tr>
                        <td>شناسه</td>
                        <td>عنوان</td>
                        <td>قیمت</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($packages as $package)
                        @include("Admin.Dashboard.Packages.PackageItem" , $package)
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    </div>
@endsection
