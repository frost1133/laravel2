
@extends("Layouts.Admin")

@section("content")
    <?php
        global $PackageData;
    $PackageData = $package;
    function packageData ($filed){
        global $PackageData;
        if ( old($filed) == null){
            if ($filed === "categories"){
                return $PackageData->categories()->pluck("id")->toArray();
            }
            return $PackageData->$filed;
        }
        return old($filed);
    }
    ?>
    <div class="panel panel-default">
        <div class="panel-heading">ویرایش پکیج</div>
        <div class="panel-body">
            <div class="row">
                <form class="col-sx-12 col-md-6"   action="{{route("admin.packages.update" , $package)}}" method="post">
                    @csrf
                    @method("PATCH")
                    @include("Admin.Partials.Errors")
                    <div class="form-group">
                        <lable for="title">عنوان پکیج:</lable>
                        <input type="text" name="title" id="title" placeholder="عنوان..." class="form-control" value="{{packageData("title")}}">
                    </div>
                    <div class="form-group">
                        <lable for="price">قیمت:</lable>
                        <input type="number" name="price" id="price" placeholder="قیمت..." class="form-control" value="{{packageData("price")}}">
                    </div>
                    <div class="form-group">
                        <lable for="categories">انتخاب دسته:</lable>
                        <select name="categories[]" id="categories" class="form-control" multiple>
                            @foreach($categories as $category)
                                <option value="{{$category->id}}" {{is_array(packageData("categories"))  && in_array($category->id , packageData("categories")) ? "selected" : ""}}>{{$category->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-success" type="submit">ثبت</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>
@endsection

@section("pageScript")
    <script>
        $(document).ready(function () {
            $("#categories").select2({
                dir: "rtl"
            });
        })
    </script>
@endsection
