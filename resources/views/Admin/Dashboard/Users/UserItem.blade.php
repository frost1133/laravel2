<tr>
    <td>{{$user->id}}</td>
    <td>{{$user->name}}</td>
    <td>{{$user->email}}</td>
    <td>{{$user->wallet}}</td>
    <td>{{$user->packages()->count()}}</td>
    <td style="text-align: center">
        <form action="{{route("admin.users.delete" , ["user" => $user->id])}}" method="post">
            @csrf
            @method("DELETE")
            <div class="btn-group">
                <a href="{{route("admin.users.edit" , ["user" => $user->id])}}" class="btn btn-success"><i class="glyphicon glyphicon-edit"></i></a>
                <button type="submit" class="btn btn-danger"><i class="glyphicon glyphicon-trash"></i></button>
                <a title="لیست پکیج های خریداری شده" href="{{route("admin.users.packages" , ["user" => $user->id])}}" class="btn btn-default"><i class="glyphicon glyphicon-list"></i></a>
            </div>
        </form>
    </td>
</tr>
