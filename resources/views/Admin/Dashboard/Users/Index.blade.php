@extends("Layouts.Admin")
@section("content")
    <div class="panel panel-default">
        <div class="panel-heading">لیست کاربران</div>
        <div class="panel-body">
            @include("Admin.Partials.Notifications")
            <table class="table table-hover">
                <thead>
                    <tr>
                        <td>شناسه</td>
                        <td>نام</td>
                        <td>ایمیل</td>
                        <td>موجودی</td>
                        <td>تعداد پکیج های</td>
                        <td>عملیات</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                        @include("Admin.Dashboard.Users.UserItem" , $user)
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    </div>
@endsection
