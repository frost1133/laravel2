@extends("Layouts.Admin")
@section("content")
    <div class="panel panel-default">
        <div class="panel-heading">لیست پکیج های خریداری شده کاربر {{$user->name}}</div>
        <div class="panel-body">
            @include("Admin.Partials.Notifications")
            <table class="table table-hover">
                <thead>
                    <tr>
                        <td>پکیج</td>
                        <td>مبلغ پرداخت شده</td>
                        <td>تاریخ خریداری شده</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($user->packages as $package)
                        <tr>
                            <td>{{$package->title}}</td>
                            <td>{{number_format($package->pivot->amount)}}</td>
                            <td>{{$package->pivot->created_at}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    </div>
@endsection
