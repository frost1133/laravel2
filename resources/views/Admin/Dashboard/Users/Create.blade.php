@extends("Layouts.Admin")
@section("content")
    <div class="panel panel-default">
        <div class="panel-heading">ثبت کاربر جدید</div>
        <div class="panel-body">
            <div class="row">
                <form class="col-sx-12 col-md-6" action="{{route("admin.users.store")}}" method="post">
                    @csrf
                    @include("Admin.Partials.Errors")
                    <div class="form-group">
                        <lable for="fullName">نام کامل:</lable>
                        <input type="text" name="name" id="fullName" placeholder="نام کامل..." class="form-control" value="{{old("name")}}">
                    </div>
                    <div class="form-group">
                        <lable for="email">ایمیل:</lable>
                        <input type="text" name="email" id="email" placeholder="ایمیل..." class="form-control" value="{{old("email")}}">
                    </div>
                    <div class="form-group">
                        <lable for="password">کلمه عبور:</lable>
                        <input type="password" name="password" id="password" placeholder="کلمه عبور..." class="form-control">
                    </div>
                    <div class="form-group">
                        <lable for="password_confirmation">تایید کلمه عبور:</lable>
                        <input type="password" name="password_confirmation" id="password_confirmation"placeholder="تایید کلمه عبور..." class="form-control">
                    </div>
                    <div class="form-group">
                        <lable for="wallet">کیف پول:</lable>
                        <input type="text" name="wallet" id="wallet"placeholder="موجودی کیف پول..." class="form-control" value="{{old("wallet")}}">
                    </div>
                    <div class="form-group">
                        <lable for="role">نقش کاربر:</lable>
                        <select class="form-control" name="role" id="role">
                            <option value="1" {{old("role") == "1" ? "selected" : ""}}>کاربر عادی</option>
                            <option value="2" {{old("role") == "2" ? "selected" : ""}}>اپراتور</option>
                            <option value="3" {{old("role") == "3" ? "selected" : ""}}>مدیریت</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-success" type="submit">ثبت</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>
@endsection
