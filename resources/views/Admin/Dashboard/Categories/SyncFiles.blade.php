@extends("Layouts.Admin")
@section("content")
    <div class="panel panel-default">
        <div class="panel-heading">ٍثبت فایل های پکیج</div>
        <div class="panel-body">
            <div class="row">
                <form class="col-sx-12 col-md-6" action="{{route("admin.packages.sync_file" , $package)}}" method="post">
                    @csrf
                    @include("Admin.Partials.Errors")
                    <div class="form-group">
                        <lable for="files">انتخاب فایل های پکیج:</lable>
                    </div>
                        <ul>
                            @foreach($files as $file)
                            <li>
                                <input type="checkbox" name="files[]" id="files"{{in_array($file->id , $packageFiles)?" checked" : ""}} value="{{$file->id}}">
                                {{$file->title}}
                            </li>
                            @endforeach
                        </ul>
                    <div class="form-group">
                        <button class="btn btn-success" type="submit">ثبت</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>
@endsection
