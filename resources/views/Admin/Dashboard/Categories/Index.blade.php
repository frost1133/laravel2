@extends("Layouts.Admin")
@section("content")
    <div class="panel panel-default">
        <div class="panel-heading">لیست پکیج ها</div>
        <div class="panel-body">
            @include("Admin.Partials.Notifications")
            <table class="table table-hover">
                <thead>
                    <tr>
                        <td>شناسه</td>
                        <td>نام دسته</td>
                        <td>عملیات</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($categories as $category)
                        @include("Admin.Dashboard.Categories.CategoryItem" , $category)
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    </div>
@endsection
