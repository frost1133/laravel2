<tr>
    <td>{{$category->id}}</td>
    <td>{{$category->name}}</td>
    <td style="text-align: center">
        <form action="{{route("admin.categories.delete" , $category)}}" method="post">
            @csrf
            @method("DELETE")
            <div class="btn-group">
                <a href="{{route("admin.categories.edit" , $category)}}" class="btn btn-success"><i class="glyphicon glyphicon-edit"></i></a>
                <button type="submit" class="btn btn-danger"><i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </form>
    </td>
</tr>
