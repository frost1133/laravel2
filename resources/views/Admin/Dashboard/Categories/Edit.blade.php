
@extends("Layouts.Admin")

@section("content")
    <?php
        global $CategoryData;
    $CategoryData = $category;
    function categoryData ($filed){
        global $CategoryData;
        if ( old($filed) == null){
            return $CategoryData->$filed;
        }
        return old($filed);
    }
    ?>
    <div class="panel panel-default">
        <div class="panel-heading">ویرایش پکیج</div>
        <div class="panel-body">
            <div class="row">
                <form class="col-sx-12 col-md-6"   action="{{route("admin.categories.update" , $category)}}" method="post">
                    @csrf
                    @method("PATCH")
                    @include("Admin.Partials.Errors")
                    <div class="form-group">
                        <lable for="name">نام دسته:</lable>
                        <input type="text" name="name" id="name" placeholder="نام دسته..." class="form-control" value="{{categoryData("name")}}">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-success" type="submit">ثبت</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>
@endsection


