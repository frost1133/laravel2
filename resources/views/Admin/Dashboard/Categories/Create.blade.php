@extends("Layouts.Admin")
@section("content")
    <div class="panel panel-default">
        <div class="panel-heading">ثبت دسته جدید</div>
        <div class="panel-body">
            <div class="row">
                <form class="col-sx-12 col-md-6" action="{{route("admin.categories.store")}}" method="post">
                    @csrf
                    @include("Admin.Partials.Errors")
                    <div class="form-group">
                        <lable for="name">نام دسته:</lable>
                        <input type="text" name="name" id="name" placeholder="نام دسته..." class="form-control" value="{{old("name")}}">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-success" type="submit">ثبت</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>
@endsection
