@extends("Layouts.Admin")
@section("content")
    <div class="panel panel-default">
        <div class="panel-heading">ثبت فایل جدید</div>
        <div class="panel-body">
            <div class="row">
                <form class="col-sx-12 col-md-6" action="{{route("admin.files.store")}}" method="post" enctype="multipart/form-data">
                    @csrf
                    @include("Admin.Partials.Errors")
                    <div class="form-group">
                        <lable for="title">عنوان فایل:</lable>
                        <input type="text" name="title" id="title" placeholder="عنوان فایل..." class="form-control" value="{{old("title")}}">
                    </div>
                    <div class="form-group">
                        <lable for="description">توضیحات:</lable>
                        <textarea name="description" id="description" placeholder="توضیحات..." class="form-control">{{old("description")}}</textarea>
                    </div>
                    <div class="form-group">
                        <lable for="categories">انتخاب دسته:</lable>
                        <select name="categories[]" id="categories" class="form-control" multiple>
                            @foreach($categories as $category)
                            <option value="{{$category->id}}" {{is_array(old("categories"))  && in_array($category->id , old("categories")) ? "selected" : ""}}>{{$category->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <lable for="file">انتخاب فایل:</lable>
                        <input type="file" name="fileData" id="file" placeholder="انتخاب فایل..." class="form-control">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-success" type="submit">ثبت</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>
@endsection
@section("pageScript")
    <script>
        $(document).ready(function () {
            $("#categories").select2({
                dir: "rtl"
            });
        })
    </script>
@endsection
