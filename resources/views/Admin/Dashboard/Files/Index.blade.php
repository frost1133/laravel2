@extends("Layouts.Admin")
@section("content")
    <div class="panel panel-default">
        <div class="panel-heading">لیست فایل ها</div>
        <div class="panel-body">
            @include("Admin.Partials.Notifications")
            <table class="table table-hover">
                <thead>
                    <tr>
                        <td>شناسه</td>
                        <td>عنوان فایل</td>
                        <td>نام فایل</td>
                        <td>نوع فایل</td>
                        <td>اندازه فایل</td>
                        <td>عملیات</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($files as $file)
                        @include("Admin.Dashboard.Files.FileItem" , $file)
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    </div>
@endsection
