<tr>
    <td>{{$file->id}}</td>
    <td>{{$file->title}}</td>
    <td>{{$file->name}}</td>
    <td>{{$file->type}}</td>
    <td>{{$file->size}}</td>
    <td style="text-align: center">
        <form action="{{route("admin.files.delete" , $file)}}" method="post">
            @csrf
            @method("DELETE")
            <div class="btn-group">
                <a href="{{route("admin.files.edit" , $file)}}" class="btn btn-success"><i class="glyphicon glyphicon-edit"></i></a>
                <button type="submit" class="btn btn-danger"><i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </form>
    </td>
</tr>
