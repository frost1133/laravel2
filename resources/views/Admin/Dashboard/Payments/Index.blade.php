@extends("Layouts.Admin")
@section("content")
	<div class="panel panel-default">
		<div class="panel-heading">لیست پرداخت ها</div>
		<div class="panel-body">
			@include("Admin.Partials.Notifications")
			<table class="table table-hover">
				<thead>
				<tr>
					<th>کاربری</th>
					<th>مبلغی</th>
					<th>نوع</th>
					<th>بانک</th>
					<th>شماره رزرو</th>
					<th>شماره ارجاع</th>
					<th>تاریخ پرداخت</th>
					<th>وضعیت</th>
					<th>عملیات</th>
				</tr>
				</thead>
				<tbody>
				@foreach($payments as $payment)
					<tr>
						<td>{{$payment->user->name}}</td>
						<td>{{number_format($payment->amount)}}</td>
						<td>{{$payment->Method_Format()}}</td>
						<td>{{$payment->gateway_name}}</td>
						<td>{{$payment->res_num}}</td>
						<td>{{$payment->ref_num}}</td>
						<td>{{$payment->created_at}}</td>
						<td>{{$payment->status}}</td>
						<td>

						</td>
					</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>
	</div>
@endsection