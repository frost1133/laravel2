@extends("Layouts.App")
@section("content")
    <div class="col-md-9 col-xs-9">
        @if($files && $files->isNotEmpty())
        <div class="panel panel-default">
            <div class="panel-heading">آخرین فایل های ثبت شده</div>
            <div class="panel-body">
                <ul>
                    @foreach($files as $file)
                    <li><a href="{{route("app.file.details" , ["file" =>$file->id])}}">{{$file->title}}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
        @endif
        @if($packages && $packages->isNotEmpty() )
        <div class="panel panel-default">
            <div class="panel-heading">آخرین پکیج های ثبت شده</div>
            <div class="panel-body">
                <ul>
                    @foreach($packages as $package)
                    <li><a href="{{route("app.package.details" , $package)}}">{{$package->title}}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
        @endif
    </div>
    <div class="col-md-3 col-xs-3">
        @include("App.Partials.ListWidget" , $widget = ["title" => "لیست دسته ها" , "route" => "app.categories.view" , "list" => $categories])
        @include("App.Partials.ListWidget" , $widget = ["title" => "بیشترین دانلود ها" , "route" => "app.file.details" , "list" => $MustDownloadedFiles])
    </div>
@endsection
