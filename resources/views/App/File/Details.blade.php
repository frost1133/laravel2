@extends("Layouts.App")
@section("content")
    <div class="col-md-9 col-xs-9">
        <div class="panel panel-default">
            <div class="panel-heading"><div class="file-icon file-icon-sm" data-type="{{$file->File_Type}}" style="float: left"></div>جزئیات فایل</div>
            <div class="panel-body">
                <h3><span>نام فایل:</span> {{$file->title}}</h3>
                <p>{{$file->description}}</p>
            </div>
        </div>
    </div>
    <div class="col-me-3 col-xs-3">
        <div class="panel panel-default">
            <div class="panel-heading">خرید فایل</div>
            <div class="panel-body">
            @if (!\Illuminate\Support\Facades\Auth::check())
                <a href="{{route("app.login.view")}}" class="btn btn-success btn-block">ورود به حساب کاربری</a>
            @elseif(\App\Utility\File::CanDownload(Auth::user(),$file))
                <a href="{{route("app.file.download" , $file)}}" class="btn btn-success btn-block">دانلود فایل</a>
                <a data-fid="{{$file->id}}" id="reportFile" class="btn btn-warning btn-block">دانلود فایل</a>
            @else
                <a href="{{route("app.plans.index")}}" class="btn btn-success btn-block">خرید طرح</a>
            @endif
            </div>
        </div>
    </div>
@endsection
@section("pageStyle")
    <link rel="stylesheet" href="/css/fileicon.css">
@endsection
@section("pageScript")
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>
        $("#reportFile").click(function (e) {

            $.ajax({
                url : "{{route("app.file.report",$file)}}",
                type : "post",
                dataType: "json",
                data : {
                    fid : $("#reportFile").data("fid"),
                    _token : "{{csrf_token()}}"
                },
                success : function (response) {
                    swal(response.title, response.message, response.type);
                },
                error : function () {
                    swal("خطا!", "عملیات ارسال گذارش با خطا مواجه شد.", "error");
                }
            });
            e.preventDefault();
        })
        $.ajax()
    </script>
@endsection
