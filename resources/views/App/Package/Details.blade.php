@extends("Layouts.App")
@section("content")
    <div class="col-md-9 col-xs-9">
        <div class="panel panel-default">
            <div class="panel-body">
                <h3><span>نام پکیج:</span> {{$package->title}}</h3>
                <h4>لیس فایل های پکیج</h4>
                <ul>
                    @foreach($package->files as $file)
                        <li><a href="{{route("app.file.download" , $file)}}">{{$file->title}}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    <div class="col-me-3 col-xs-3">
        <div class="panel panel-default">
            <div class="panel-heading">پکیج</div>
            <div class="panel-body">
            @if(! \App\Utility\Package::HavePackage($package))
                <form action="{{route("app.package.buy" , $package)}}" method="post">
                    <h3><span>قیمت پکیج:</span> {{$package->price}}</h3>
                    @csrf
                    <button type="submit" class="btn btn-success btn-block">خرید پکیج</button>
                </form>
            @endif
            </div>
        </div>
    </div>
@endsection
@section("pageStyle")
    <link rel="stylesheet" href="/css/fileicon.css">
@endsection
@section("pageScript")
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@endsection
