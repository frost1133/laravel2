@extends("Layouts.App")
@section("content")
	<div class="col-md-9 col-xs-9">
		<div class="panel panel-default">
			<div class="panel-heading">لیست فایل های دسته {{$category->name}}</div>
			<div class="panel-body">
				<ul>

					@foreach($category->files as $file)
						<li><a href="{{route("app.file.details" , $file)}}">{{$file->title}}</a></li>
					@endforeach
				</ul>
			</div>
		</div>
	</div>
	<div class="col-me-3 col-xs-3">
		<div class="panel panel-default">
			<div class="panel-heading"></div>
			<div class="panel-body">

			</div>
		</div>
	</div>
@endsection