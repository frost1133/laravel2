<div class="panel panel-default">
	<div class="panel-heading">{{$widget["title"]}}</div>
	<div class="panel-body">
		<ul>
			@foreach ($widget["list"] as $item)
			    <li><a href="{{route($widget["route"] , $item)}}">{{ isset($item->title) ? $item->title : $item->name}}</a></li>
			@endforeach
		</ul>
	</div>
</div>