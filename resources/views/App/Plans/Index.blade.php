@extends("Layouts.App")
@section("content")
    <div class="col-md-12 col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading">لیست طرح ها</div>
            <div class="panel body">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <td>عنوان طرح</td>
                            <td>قیمت</td>
                            <td>تعداد روز</td>
                            <td>محدودیت دانلود روزانه</td>
                            <td>عملیات</td>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($plans as $plan)
                        <tr>
                            <td>{{$plan->name}}</td>
                            <td>{{$plan->price}}</td>
                            <td>{{$plan->days_count}}</td>
                            <td>{{$plan->limit_download_count}}</td>
                            <td>
                                <a href="{{route("app.payment.start" , $plan)}}" class="btn btn-success">خرید طرح</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
