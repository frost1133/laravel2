import VueRouter from 'vue-router';
import AppLayout from "./components/App/Layout.vue";
    import Home from "./components/App/Home.vue";
    import FileDetails from "./components/App/File/Details.vue";

const routes = [
    {
        name:'App',
        path:'',
        component: AppLayout,
        children:[
            {
                name: "home",
                path: '',
                component: Home,
            },
            {
                name: 'file',
                path: '/file/:fileId',
                component: FileDetails
            },
            {
                name: 'category',
                path: '/category/:categoryId',
            },
            {
                name: 'package',
                path: '/package/:packageId',
            }
        ]
    }
]


export default new VueRouter({
	mode: 'history',
	routes

});