
import "bootstrap"

import Vue from 'vue';
import axios from 'axios';


window.axios = axios;
window.Vue = Vue;


window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

