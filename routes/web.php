<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(["as" => "admin." , "namespace" => "Admin" , "prefix" => "admin/" , "middleware" => ["auth" , "admin"]],function (){

	Route::get("" , "DashboardController@index")->name("dashboard");

    //َAdmin Users Routes//
    Route::group(["as" => "users." , "prefix" => "users/"] , function () {
        Route::get("" , "UserController@index")->name("list");
        Route::get("create" , "UserController@create")->name("create");
        Route::post("create" , "UserController@store")->name("store");
        Route::get("{user}/edit" , "UserController@edit")->name("edit");
        Route::get("{user}/packages" , "UserController@packages_list")->name("packages");
        Route::patch("{user}/update" , "UserController@update")->name("update");
        Route::delete("{user}/delete" , "UserController@destroy")->name("delete");
    });

    //Admin Files Routes//
    Route::group(["as" => "files." , "prefix" => "files/"] , function () {
        Route::get("", "FileController@index")->name("list");
        Route::get("create", "FileController@create")->name("create");
        Route::post("create", "FileController@store")->name("store");
        Route::get("{file}/edit", "FileController@edit")->name("edit");
        Route::patch("{file}/update", "FileController@update")->name("update");
        Route::delete("{file}/delete", "FileController@destroy")->name("delete");
    });

    //Admin Plans Routes//
    Route::group(["as" => "plans." , "prefix" => "plans/"] , function () {
        Route::get("" , "PlanController@index")->name("list");
        Route::get("create" , "PlanController@create")->name("create");
        Route::post("create" , "PlanController@store")->name("store");
        Route::get("{plan}/edit" , "PlanController@edit")->name("edit");
        Route::patch("{plan}/update" , "PlanController@update")->name("update");
        Route::delete("{plan}/delete" , "PlanController@destroy")->name("delete");
    });

    //Admin Categories Routes//
    Route::group(["as" => "categories." , "prefix" => "categories/"] , function () {
        Route::get("" , "CategoryController@index")->name("list");
        Route::get("create" , "CategoryController@create")->name("create");
        Route::post("create" , "CategoryController@store")->name("store");
        Route::get("{category}/edit" , "CategoryController@edit")->name("edit");
        Route::patch("{category}/update" , "CategoryController@update")->name("update");
        Route::delete("{category}/delete" , "CategoryController@destroy")->name("delete");
    });

    //Admin Packages Routes//
    Route::group(["as" => "packages." , "prefix" => "packages/"] , function () {
        Route::get("" , "PackageController@index")->name("list");
        Route::get("create" , "PackageController@create")->name("create");
        Route::post("create" , "PackageController@store")->name("store");
        Route::get("{package}/edit" , "PackageController@edit")->name("edit");
        Route::patch("{package}/update" , "PackageController@update")->name("update");
        Route::delete("{package}/delete" , "PackageController@destroy")->name("delete");

        Route::get('sync_files/{package}', "PackageController@sync_file")->name("sync_file_view");
        Route::post('sync_files/{package}', "PackageController@sync")->name("sync_file");
    });

    //Admin Plans Routes//
    Route::group(["as" => "payments." , "prefix" => "payments/"] , function () {
        Route::get("", "PaymentController@index")->name("list");
    });
});

Auth::routes();
Route::group(["as" => "app." , "namespace" => "App"] , function (){
	Route::middleware('guest')->group(function (){
	Route::get("Account/login" ,"AuthController@loginView")->name("login.view");
	Route::get("Account/register" ,"AuthController@registerView")->name("register.view");
	Route::post("Account/login" ,"AuthController@login")->name("login.action");
	Route::post("Account/register" ,"AuthController@register")->name("register.action");
});
	Route::get("Account/logout" ,"AuthController@logout")->name("logout.action");

    Route::get("" , "HomeController@index")->name("home");

    Route::get("/plans" , "PlanController@index")->name("plans.index");

    Route::get("/subscribe/{plan}" , "SubscribeController@subscribe")->name("subscribes.subscribe");

    Route::get("/file/{file}" , "FileController@details")->name("file.details");
    Route::get("/file/download/{file}" , "FileController@download")->name("file.download");
    Route::post("/file/report" , "FileController@report")->name("file.report");

    Route::get("/package/{package}" , "PackageController@details")->name("package.details");
    Route::post("/package/{package}" , "PackageController@buy")->name("package.buy");

    Route::get("/categories/{category}" , "CategoryController@view")->name("categories.view");

	Route::get('/payment/{plan}' ,'PaymentController@RedirectTo')->name('payment.start');
    Route::post('/payment/mellat/verify' , 'PaymentController@verify')->name('gateway.mellat.verify');
});



Route::get('/home', 'HomeController@index')->name('home');
