<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([ 'middleware' => 'api', 'prefix' => '/v1/auth' , 'namespace' => 'Api\v1' ], function ($router) {
	Route::post('login', 'AuthController@login');
	Route::post('logout', 'AuthController@logout');
	Route::post('refresh', 'AuthController@refresh');
	Route::post('me', 'AuthController@me');
});

Route::group(['namespace' => 'Api\V1\App' ], function () {
	Route::get('/HomeWidgetData', "WidgetController@HomeWidgetData" );
	Route::get('/Categories', "WidgetController@Categories" );
	Route::get('/MustDownloads', "WidgetController@MustDownloads" );
	Route::get('/LatestPackages', "WidgetController@LatestPackages" );
	Route::get('/LatestFiles', "WidgetController@LatestFiles" );

	Route::group(['prefix' => '/file'], function () {
		Route::get('/{file}', 'FileController@Details');
	});
});
