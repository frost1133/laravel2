<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use mysql_xdevapi\BaseResult;

/**
 * App\Payment
 *
 * @property int $id
 * @property int $user_id
 * @property int $method
 * @property int $amount
 * @property int $status
 * @property string $gateway_name
 * @property string $res_num
 * @property string $ref_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\User $User
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereGatewayName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereMethod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereRefId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereResNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereUserId($value)
 * @mixin \Eloquent
 */
class Payment extends Model
{
	const ONLINE = 1;
	const WALLET= 2;

	const COMPLETE = 1;
	const INCOMPLETE = 2;
    protected $guarded = ["id"];

	public function User()
	{
		return $this->belongsTo(User::class);
    }

	public function Method_Format()
	{
		switch ($this->attributes["method"]) {
			case Self::ONLINE :
				return 'آنلاین';
				break;
			case self::WALLET :
				return 'کیف پول';
				break;
		}
    }

}
