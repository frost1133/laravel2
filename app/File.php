<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\File
 *
 * @property int $id
 * @property string $title
 * @property string $name
 * @property string $description
 * @property string $type
 * @property int $size
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $report_count
 * @property int $download_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $DownloadMeUsers
 * @property-read int|null $download_me_users_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Category[] $categories
 * @property-read int|null $categories_count
 * @property-read mixed $file_type
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Package[] $packages
 * @property-read int|null $packages_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $usersDownloaded
 * @property-read int|null $users_downloaded_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\File newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\File newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\File query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\File whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\File whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\File whereDownloadCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\File whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\File whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\File whereReportCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\File whereSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\File whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\File whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\File whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class File extends Model
{
    protected $fillable = [
        "title" , "name" , "description" ,"type" , "size" , "report_count"
    ];
	/**
	 * @var mixed
	 */

	public function packages()
    {
        return $this->belongsToMany(Package::class);
    }

	public function categories()
	{
		return $this->morphToMany(Category::class,"categorizeable");
    }

    public function getFileTypeAttribute()
    {
        preg_match("/[a-zA-Z0-9]+$/" , $this->attributes["name"] , $fileType);

        return $fileType[0];
    }

	public static function PopulerDownloaded()
	{
		return File::orderBy("download_count" , "desc")->limit(5)->get();
    }

	public function usersDownloaded()
	{
		return $this->belongsToMany(User::class , UserDownload::class)->withPivot("download_date");
    }

    public function DownloadMeUsers()
    {
        return $this->belongsToMany(User::class , "user_downloads")->withPivot("download_date");
    }
}
