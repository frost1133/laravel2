<?php

namespace App\Mail;

use App\Subscribe;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserSubscribed extends Mailable
{
    use Queueable, SerializesModels;
	protected $subscribed ;

	/**
	 * Create a new message instance.
	 *
	 * @param Subscribe $subscribe
	 */
    public function __construct(Subscribe $subscribe)
    {
        $this->subscribed = $subscribe;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('Mails.Subscribed')->with("subscribed" , $this->subscribed);
    }
}
