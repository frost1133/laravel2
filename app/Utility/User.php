<?php


namespace App\Utility;


use App\Subscribe;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use phpDocumentor\Reflection\Types\Integer;

class User
{
    public static function UserSubscribed($user = null)
    {
        if ($user === null){
            $user = Auth::user();
        }elseif(is_integer($user)){
            $user = \App\User::find($user);
        }
        $subscribed = $user->ActivePlan();
        return !empty($subscribed) && ($subscribed instanceof Subscribe) ? $subscribed : false;
    }

	public static function DownloadableToday($user = null , $file = null)
	{
		/** @var TYPE_NAME $fup */
		$fup = File::CanDownload($user , $file);

	    if ($fup instanceof Subscribe){
	        return ($fup->download_count < $fup->download_limit)? $fup : false;
        }else{
	        return false;
        }
    }
}
