<?php


namespace App\Utility;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class File
{

	public static function GetParamsFileLink($file){
		$PaternDirectory = '/(\/|\w+)(\/(.+\/))/';
		$patern = '/((?<Directory>\w+)(?=\/))|(?<FileName>\w+)(?=\.)|((?<=\.)(?<FileExtension>\w+))/';

		preg_match_all($patern , $file , $matches );
		preg_match($PaternDirectory , $file , $Directory);
		foreach ($matches as $index => $match){
			if(is_integer($index)) {
				unset($matches[$index]);
			}else {
				foreach ($match as $i => $m) {
					if ($m == ""){
						unset($matches[$index][$i]);
					}
				}
				$matches[$index] = array_values($matches[$index]);
			}
		}
		$matches["FileLink"] = $file;
		$matches["FileDirectory"] = $Directory;
//		dd($matches);
		return $matches;
	}

	public static function GenerateFileSizeName($File , $Size , $Storage = null ){
		if (is_string($File)){
			$File= self::GetParamsFileLink($File);
		}
		$OriginalFileName = $File['FileName'][0].'.'.$File['FileExtension'][0];
		$FileSizeName = $File['FileName'][0].'__'.$Size.'.'.$File['FileExtension'][0];
		$Directory = str_replace('/' , '\\' , $File['FileDirectory'][3]);
		$File['FileDirectory'][] = $Directory;
		$File['OriginalFileName'] = $OriginalFileName;
		$File['FileSizeName'] = $FileSizeName;
		$File['Storage'] = $Storage == null? 'upload':$Storage;
		$File['Size'] = $Size;
//		dd($File);
		return $File;
	}

	public static function ExistSize($File , $Size , $Storage = null ){
		if (is_string($File)){
			$File= self::GenerateFileSizeName($File , $Size , $Storage);
		}
		$Driver = Storage::drive($File['Storage']);
		return $Driver->exists($File['FileDirectory'][4].$File['FileSizeName']) ? $Driver->url($File['FileDirectory'][3].$File['FileSizeName']):false;
	}

	public static function MakeFileSize ($File , $Size , $Storage = null)
	{
		if (is_string($File)) {
			$File = self::GenerateFileSizeName($File, $Size, $Storage);
		}
		$FilePath = Storage::drive($File['Storage'])->path($File['FileDirectory'][4]);
		$Image = Image::make($FilePath . $File['OriginalFileName']);
		$ImageRatio = $Image->getHeight() / $Image->getWidth();
		$NewHeight = round($Size * $ImageRatio);
		$Image->resize($Size, $NewHeight)->save($FilePath . $File['FileSizeName']);

	}

	public static function GetFileSizeLink($File , $Size , $Storage = null){
		if (is_string($File)) {
			$File = self::GenerateFileSizeName($File, $Size, $Storage);
		}
		if (!self::ExistSize($File,$Size)){
			self::MakeFileSize($File , $Size , $Storage);
		}
		return self::ExistSize($File,$Size);
	}

	public static function Upload(UploadedFile $file ,array $sizes){
		$file = $file->move(storage_path('upload\Pictures') , $file->getClientOriginalName());
		preg_match('/^(.*)(\.(.*))$/' , $file->getBasename() , $fileName);
		foreach ($sizes as $size){
			dd(Storage::drive('upload')->url('Pictures/'.$file->getFilename()));
			$newName = $fileName[1].'_'.$size.$fileName[2];
			$path= storage_path('upload\Pictures\\');
			Image::make($file->getRealPath())->resize($size,$size)->save($path.$newName);

		}
	}


	public static function CanDownload($user = null , $file = null )
    {
        if ($user === null){
            $user = Auth::user();
        }elseif(is_integer($user)){
            $user = \App\User::find($user);
        }

        if ($file == null){
            return User::UserSubscribed($user);
        }elseif(is_integer($file)){
            $file = \App\File::find($file);
        }
		$userPackages = $user->packages;
        $filePackages = $file->packages;
        if ($filePackages->isEmpty()){
            return User::UserSubscribed($user);
        }elseif($userPackages->isEmpty()){
			return User::UserSubscribed($user);
		}

        if ($userPackages->isNotEmpty() && $filePackages->intersect($userPackages)){
            return $filePackages->intersect($userPackages)[0];
        }
		dd($userPackages);
        return false;

    }
}

    public static function Upload(UploadedFile $file , $sizes){

		$file = $file->move(storage_path('upload\product') , $file->getClientOriginalName());
		preg_match('/^(.*)(\.(.*))$/', $file->getFilename() , $fileName);
    	$path = $file->getPath();
		foreach($sizes as $size){
    		$newName = $fileName[1].'__'.$size.$fileName[2];
			Image::make($file->getRealPath())->resize($size,$size)->save($path.'\\'.$newName);
			dd(\Storage::drive('upload')->url('product/'.$newName));
		}
	}
}