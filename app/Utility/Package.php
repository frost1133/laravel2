<?php


namespace App\Utility;


use Illuminate\Support\Facades\Auth;

class Package
{
    public static function HavePackage($package)
    {
        if (!$package instanceof \App\Package){
            $package = \App\Package::find($package);
        }
        if (!Auth::check()){
            return false;
        }

        $userPackages = Auth::user()->packages;
        if ($userPackages->isNotEmpty() && $userPackages->contains($package)){
            return $package;
        }
        return false;
    }
}
