<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use App\Plan;
use Illuminate\Http\Request;

class PlanController extends Controller
{
    public function index()
    {
        $plans = Plan::all();
        return view("App.Plans.Index" , compact('plans'))->with("title" , "لیست طرح ها");
    }
}
