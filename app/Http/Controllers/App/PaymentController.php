<?php

namespace App\Http\Controllers\App;

use App\Payment;
use App\Plan;
use App\Services\Payment\Mellat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PaymentController
{
	private $melat;

	public function __construct() {
		$this->melat = new Mellat();
	}

	public function RedirectTo (Plan $plan){
		$data = [
			'userId'    =>  Auth::id(),
			'amount'    =>  '0',
			'orderId'   =>  time().random_int(3,5).Auth::id()
		];

		$this->melat->DoPayment($data);

    }

	public function Verifty(Request $request)
	{
		if ($request->has('ResCpde')){

			if ($request->input('ResCode')){
				$params = [
					'ResCode'		=>	$request->input('ResCode'),
					'SaleOrderId'		=>	$request->input('SaleOrderId'),
					'SaleReferenceId'		=>	$request->input('SaleReferenceId'),
				];
				$verifyResult = $this->melat->VerifyPayment($params);
				if ($verifyResult){

				}
			}
		}
    }

}
