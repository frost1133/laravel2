<?php

namespace App\Http\Controllers\App;

use App\Events\RegisteredUser;
use App\Http\Controllers\Controller;
use App\Jobs\SendEmailNotify;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\MessageBag;

class AuthController extends Controller
{
	public function loginView()
	{
		return view("App.Auth")->with("title" , "ورود به حساب کاربری");
    }

	public function registerView()
	{
		return view("App.Auth")->with("title" , "ثبت نام");
    }

	public function login(Request $request)
	{
		$remember = $request->remember ? true : false;
		if (Auth::attempt(['email' => $request->input("email") , 'password' => $request->input("password")],$remember)){
			$job = (new SendEmailNotify(Auth::user()))->onQueue("Email");
			$this->dispatch($job);
			return redirect("/");
		}
		$error = new MessageBag();
		$error->add("login" , "نام کاربری یا کلمه عبور یا هردو اشتباه است.");
		return back()->withErrors($error);
    }

	public function register(Request $request)
	{
		$request->validate([
			"name" => "required|min:5",
			"email" => "required|email|max:200|unique:users",
			"password" => "required|min:6|confirmed"
		]);

		$user = User::create([
			"name" => $request->input("name"),
			"email" => $request->input("email"),
			"password" => $request->input("password"),
			]);
		if ($user instanceof User){
			event(new RegisteredUser($user));
			return redirect()->route("app.login.view");
		}
		return redirect()->route("app.register.view");
    }

	public function logout()
	{
		Auth::logout();
		return redirect()->route("app.home");
    }
}
