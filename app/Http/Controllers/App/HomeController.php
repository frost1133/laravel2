<?php

namespace App\Http\Controllers\App;

use App\Category;
use App\File;
use App\Http\Controllers\Controller;
use App\Package;
use App\UserDownload;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
    	return view('VueIndex');
        $packages = Package::all();
        $files = File::all();
        $categories = Category::all();
        $MustDownloadedFiles = File::PopulerDownloaded();
        return view("App.Index" , compact(["packages" , "files" , "categories" , "MustDownloadedFiles"]))->with(["title" => "صفحه اصلی"]);
    }
}
