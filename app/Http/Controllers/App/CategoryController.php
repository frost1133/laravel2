<?php

namespace App\Http\Controllers\App;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
	public function view(Category $category)
	{
		return view("App.Categories.View")->with(["category" => $category , "title" => "دسته {$category->name}"]);
    }
}
