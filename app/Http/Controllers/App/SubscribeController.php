<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use App\Mail\UserSubscribed;
use App\Plan;
use App\Subscribe;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class SubscribeController extends Controller
{
    public function subscribe(Plan $plan)
    {
        $user = Auth::user();
        $subscribed =  $plan->subscribes()->create([
            "user_id"           =>  $user->id,
            "download_limit"    =>  $plan->limit_download_count,
            "download_count"    =>  0,
            "payment_amount"    =>  $plan->price,
            "expired_at"        =>  Carbon::now()->addDay($plan->days_count)
        ]);
        Mail::to($subscribed->user)->send( new UserSubscribed($subscribed));
        return redirect()->route("app.home");
    }
}
