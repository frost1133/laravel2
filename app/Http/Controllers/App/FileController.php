<?php

namespace App\Http\Controllers\App;

use App\File;
use App\Http\Controllers\Controller;
use App\Subscribe;
use App\Utility\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class FileController extends Controller
{
    public function details(File $file)
    {
        return view("App.File.Details")->with(["file" => $file , "title" => "جزئیات فایل {$file->title}"]);
    }

    public function download(File $file)
    {
        $user = Auth::user();
        $f = \App\Utility\File::CanDownload($user,$file);
        if ($f){
            $fileDirectory = storage_path("upload").'\files\\';
            $filePath = $fileDirectory.$file->name;
            if (Storage::disk("upload")->exists('\files\\'.$file->name)){
                if ($f instanceof Subscribe) {
                    if (!User::DownloadableToday($user,$file)){
                        return back();
                    }
                    $f->increment("download_count");
                }
				$file->DownloadMeUsers()->attach($user , ["download_date" => Carbon::now()]);
				$file->increment("download_count");
                return response()->download($filePath);
            }
            return back();
        }
        return back();
    }

    public function report(Request $request)
    {
        $file = File::find($request->fid);
        $res =  $file->increment("report_count");
        if (is_integer($res)){
            return response()->json(["type" => "success" , "message" => "گذارش شما با موفقیت ثبت گردید." , "title" => "موفق"]);
        }else{
            return response()->json(["type" => "error" , "message" => "عملیا ثبت گذارش با خطا مواجه شد!" , "title" => "خطا!"]);
        }
    }
}
