<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use App\Package;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PackageController extends Controller
{
    public function details(Package $package)
    {

        return view("App.Package.Details")->with(["package" => $package , "title" => $package->title]);
    }

    public function buy(Package $package)
    {
        if (!Auth::check()){
            return redirect()->route("app.login.view");
        }
        Auth::user()->packages()->attach($package , ["amount" => $package->price]);
        return back();
    }
}
