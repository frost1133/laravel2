<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Payment;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
	public function index()
	{
		$payments = Payment::all();
		return view("Admin.Dashboard.Payments.Index")->with("payments" , $payments);
    }
}
