<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Plan;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class PlanController extends Controller
{
    protected $rule = [
        "name" => "required|max:150|min:5",
        "limitD" => "required|numeric",
        "price" => "required|numeric",
        "days" => "required|numeric",
    ];
    public function index()
    {
        $plans = Plan::all();
        return view("Admin.Dashboard.plans.Index" )->with(["plans" => $plans]);
    }

    public function create()
    {
        return view("Admin.Dashboard.plans.Create");
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate($this ->rule);
        $planCreated = Plan::create([
            'name'                  =>  $request->input("name"),
            'limit_download_count'  =>  $request->input("limitD"),
            'price'                 =>  $request->input("price"),
            'days_count'            =>  $request->input("days"),
        ]);

        if ($planCreated instanceof Plan){
            return redirect()->route("admin.plans.list")->with(["success" => "طرح جدید {$planCreated->name} با موفقیت ثبت گردید."]);
        }
    }

    public function edit(Plan $plan)
    {
        return view("Admin.Dashboard.plans.Edit" )->with("plan" , $plan);
    }

    public function update(Plan $plan , Request $request)
    {
        $request->validate($this ->rule,[
            "days.required" => "تعداد روز اعتبار الزامی است.",
            "days.numeric" => "تعداد روز اعتبار باید عدد یا رشته‌ای از اعداد باشد.",
            "price.required" => "قیمت الزامی است.",
            "price.numeric" => "قیمت باید عدد یا رشته‌ای از اعداد باشد.",
            "limitD.required" => "محدودیت دانلود روزانه الزامی است.",
            "limitD.numeric" => "محدودیت دانلود روزانه باید عدد یا رشته‌ای از اعداد باشد.",
        ]);

        $plan->name                  =  $request->input("name");
        $plan->limit_download_count  =  $request->input("limitD");
        $plan->price                 =  $request->input("price");
        $plan->days_count            =  $request->input("days");
        $plan->save();
        return redirect()->route("admin.plans.list")->with(["success" => "طرح  {$plan->name} با موفقیت بروزرسانی گردید."]);
    }

    public function destroy(Plan $plan)
    {
        $name = $plan->name;
        $plan->delete();
        return redirect()->route("admin.users.list")->with(["success" => "طرح  {$name} با موفقیت حذف گردید."]);
    }
}
