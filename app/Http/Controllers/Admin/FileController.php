<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class FileController extends Controller
{
    public function index()
    {
        $files = File::all();
        return view("Admin.Dashboard.Files.Index")->with("files" , $files);
    }

    public function store(Request $request)
    {
        $request->validate([
            "title" => "required",
            "fileData" => "required",
            "categories" => "required|array"
        ],[
            "fileData.required" => "آپلود کردن فایل الزامی است",
            "categories.required" => "انتخاب دسته الزامی است.",
            "categories.array" => "دسته ها باید به صورت آرایه ارسال گردند."
        ]);

        $file = $request->file('fileData');
        $name = $file->getClientOriginalName();
		preg_match('/^(.*)(\.(.*))$/', $name , $matchs);
		\App\Utility\File::Upload($file , [300,600]);
        $file = $file->move(storage_path('upload\files') , $file->hashName());
        if ($file instanceof \Symfony\Component\HttpFoundation\File\File){
            $fileData =  File::create([
                'title'         =>  $request->input("title"),
                'description'   =>  $request->input("description") ? $request->input("description") : " ",
                'type'          =>  $file->getMimeType(),
                'size'          =>  $file->getSize(),
                'name'          =>  $file->getBasename()
            ]);
            $fileData->categories()->sync($request->input("categories"));
            return redirect()->route('admin.files.list')->with('success' , "فایل ( {$fileData->title} ) با موفقیت ثبت گردید.");
        }
        return back();
    }

    public function create()
    {
    	$categories = Category::all();
        return view("Admin.Dashboard.Files.Create")->with("categories" , $categories);
    }

    public function edit(File $file)
    {
        $categories = Category::all();
        return view("Admin.Dashboard.Files.Edit")->with(['file' => $file , "categories" => $categories]);
    }

    public function update(File $file , Request $request)
    {
        $request->validate([
            "title" => "required",
            "categories" => "required|array"
        ],[
            "categories.required" => "انتخاب دسته الزامی است.",
            "categories.array" => "دسته ها باید به صورت آرایه ارسال گردند."
        ]);
        $fileData = [
            'title'         =>  $request->input("title"),
            'description'   =>  $request->input("description") ? $request->input("description") : " "
            ];
        if ($request->file('fileData')) {
            $fileUploaded = $request->file('fileData');
            \App\Utility\File::Upload($fileUploaded , [300,600,1900]);
            $fileUploaded = $fileUploaded->move(storage_path('upload\files'), $fileUploaded->hashName());
            if ($fileUploaded instanceof \Symfony\Component\HttpFoundation\File\File) {
            	$existFile = Storage::disk('upload')->exists("/files/".$file->name);
                $delFile = Storage::disk('upload')->delete('files/' . $file->name);
                if ($delFile == true or  $existFile == false){
                    $fileData['type'] = $fileUploaded->getMimeType();
                    $fileData['size'] = $fileUploaded->getSize();
                    $fileData['name'] = $fileUploaded->getBasename();
                }
            }
        }
        $file->update($fileData);
        $file->categories()->sync($request->input("categories"));
        return redirect()->route('admin.files.list')->with('success' , "فایل ( {$request->input("title")} ) با موفقیت آپدیت شد.");
    }

    public function destroy(File $file)
    {
         $delFile = Storage::disk('upload')->delete('files/'.$file->name);
         if ($delFile == true){
             $fileTitle = $file->title;
             $file->delete();
             return back()->with("success" , "فایل ( {$fileTitle} ) با موفقیت حذف گردید.");
         }
    }
}
