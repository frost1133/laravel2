<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use App\UserDownload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Intervention\Image\ImageManager;

class DashboardController extends Controller
{
    public function index()
    {
    	$file = '/upload/Pictures/Untitledd.png';

    	dd(\App\Utility\File::GetFileSizeLink($file , 400));


        $DaysDownloads = UserDownload::DaysDownloads();

        return view("Admin.Dashboard.Index" , compact("DaysDownloads"));
    }
}
