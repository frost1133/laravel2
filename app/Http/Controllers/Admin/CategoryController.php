<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
	protected $rule = ["name" => "required"];
	public function index()
	{
		$categories = Category::all();
		return	view("Admin.Dashboard.Categories.Index")->with("categories" , $categories);
    }

	public function create()
	{
		return view("Admin.Dashboard.Categories.Create");
    }

	public function store(Request $request)
	{
		$request->validate($this ->rule);
		$categoryCreated = Category::create([
			'name'	=>  $request->input("name")
		]);

		if ($categoryCreated instanceof Category){
			return redirect()->route("admin.categories.list")->with(["success" => "دسته جدید {$categoryCreated->title} با موفقیت ثبت گردید."]);
		}
    }

    public function edit (Category $category){
		return view("Admin.Dashboard.Categories.Edit")->with("category" , $category);
	}

	public function update(Category $category , Request $request)
	{
		$request->validate($this->rule);

		$category->name = $request->input("name");
		$category->save();
		return redirect()->route("admin.categories.list")->with(["success" => "دسته  {$category->name} با موفقیت بروزرسانی گردید."]);
	}

	public function destroy(Category $category)
	{
		$name = $category->name;
		$category->delete();

		return redirect()->route("admin.categories.list")->with(["success" => "دسته   {$name} با موفقیت حذف گردید."]);
	}

}
