<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\File;
use App\Http\Controllers\Controller;
use App\Package;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class PackageController extends Controller
{
    protected $rule = [
        "title" => "required|max:150|min:5",
        "price" => "required|numeric",
    ];
    public function index()
    {
        $packages = Package::all();
        return view("Admin.Dashboard.packages.Index" )->with(["packages" => $packages]);
    }

    public function create()
    {
		$categories = Category::all();
        return view("Admin.Dashboard.packages.Create")->with("categories" , $categories);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate($this ->rule , [
            "price.required" => "قیمت الزامی است.",
            "price.numeric" => "قیمت باید عدد یا رشته‌ای از اعداد باشد.",
			"categories" => "required|array",
        ],[
			"categories.required" => "انتخاب دسته الزامی است.",
			"categories.array" => "دسته ها باید به صورت آرایه ارسال گردند."
		]);
        $packageCreated = Package::create([
            'title'                  =>  $request->input("title"),
            'price'                  =>  $request->input("price"),
        ]);
		$packageCreated->categories()->sync($request->input("categories"));
        if ($packageCreated instanceof Package){
            return redirect()->route("admin.packages.list")->with(["success" => "پکیج جدید {$packageCreated->title} با موفقیت ثبت گردید."]);
        }
    }

    public function edit(Package $package)
    {
		$categories = Category::all();
        return view("Admin.Dashboard.packages.Edit" )->with(["package" => $package , "categories" => $categories]);
    }

    public function update(Package $package , Request $request)
    {
        $request->validate($this ->rule , [
            "price.required" => "قیمت الزامی است.",
            "price.numeric" => "قیمت باید عدد یا رشته‌ای از اعداد باشد.",
			"categories" => "required|array",
        ],[
			"categories.required" => "انتخاب دسته الزامی است.",
			"categories.array" => "دسته ها باید به صورت آرایه ارسال گردند."
		]);

        $package->title  =  $request->input("title");
        $package->price  =  $request->input("price");
        $package->save();
		$package->categories()->sync($request->input("categories"));
        return redirect()->route("admin.packages.list")->with(["success" => "پکیج  {$package->name} با موفقیت بروزرسانی گردید."]);
    }

    public function destroy(Package $package)
    {
        $title = $package->title;
        $package->delete();
        return redirect()->route("admin.packages.list")->with(["success" => "پکیج   {$title} با موفقیت حذف گردید."]);
    }

	public function sync_file(Package $package)
	{
		$files = File::all();
		$packageFiles = $package->files()->pluck("id")->toArray();
		return view("Admin.Dashboard.Packages.SyncFiles")->with(["files" => $files , "package" => $package , "packageFiles" => $packageFiles]);
    }

	public function sync(Package $package , Request $request)
	{
		$filesSelected = $request->input("files");

		$package->files()->sync($filesSelected);

		return redirect()->route("admin.packages.list")->with("success" , "فایل های پکیج ( {$package->title} ) با موفقیت ثبت گردید.");
    }
}
