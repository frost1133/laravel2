<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();
        return view("Admin.Dashboard.Users.Index" )->with(["users" => $users]);
    }

    public function create()
    {
        return view("Admin.Dashboard.Users.Create");
    }

    /**
     * @param UserRequest $request
     * @return RedirectResponse
     */
    public function store(UserRequest $request)
    {

        $userCreated = User::create([
            'name'      =>  $request->name,
            'email'     =>  $request->email,
            'role'      =>  $request->role,
            'wallet'    =>  $request->wallet,
            'password'  =>  $request->password,
        ]);

        if ($userCreated instanceof User){
            return redirect()->route("admin.users.list")->with(["success" => "کاربر جدید {$userCreated->name} با موفقیت ثبت گردید."]);
        }
    }

    public function edit(User $user)
    {
        return view("Admin.Dashboard.Users.Edit" )->with("user" , $user);
    }

    public function update(User $user , UserRequest $request)
    {
        $user->name = $request->name;
        $user->email = $request->email;
        $user->role = $request->role;
        $user->wallet = $request->wallet;
        if ($request->password){
        	$user->password = $request->password;
		}
        $user->save();
        return redirect()->route("admin.users.list")->with(["success" => "کاربر {$user->name} با موفقیت بروزرسانی گردید."]);
    }

    public function destroy(User $user)
    {
        $name = $user->name;
        $user->delete();
        return redirect()->route("admin.users.list")->with(["success" => "کاربر {$name} با موفقیت حذف گردید."]);
    }

	public function packages_list(User $user)
	{
		return view("Admin.Dashboard.Users.Packages")->with("user" , $user);
    }
}
