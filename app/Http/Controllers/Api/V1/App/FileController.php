<?php

namespace App\Http\Controllers\Api\v1\App;

use App\File;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FileController extends Controller
{
	public function Details(File $file)
	{
		$file->__set("FileType" , $file->file_type);

		return $file;
    }
}
