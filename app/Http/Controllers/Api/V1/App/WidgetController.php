<?php

namespace App\Http\Controllers\Api\V1\App;

use App\Category;
use App\File;
use App\Http\Controllers\Controller;
use App\Package;
use Illuminate\Http\Request;
use Psy\Util\Json;

class WidgetController extends Controller
{
	public function HomeWidgetData(){
		$home['Categories'] = self::Categories();
		$home['LatestFiles'] = self::LatestFiles();
		$home['LatestPackages'] = self::LatestPackages();
		$home['MustDownloads'] = self::MustDownloads();
		return $home;
	}
	public function Categories()
	{
		$Categories = [];
		foreach (Category::all() as $cat){
			$catjson = new Json();
			$catjson->id = $cat->id;
			$catjson->name = $cat->name;
			$catjson->route = 'category';
			$catjson->link = route('app.categories.view' , $cat);
			$Categories[] = $catjson;
		}
		return $Categories;
	}

	public function LatestPackages()
	{
		$packages = Package::orderBy('created_at' , 'desc')->limit(5)->get(['title' , 'id']);
		$LatestPackages = [];
		foreach ($packages as $package){
			$packagejson = new Json();
			$packagejson->id = $package->id;
			$packagejson->title = $package->title;
			$packagejson->route = 'package';
			$packagejson->link = route('app.package.details' , $package);
			$LatestPackages[] = $packagejson;
		}
		return $LatestPackages;
	}

	public function LatestFiles()
	{
		$files = File::orderBy('created_at' , 'desc')->limit(5)->get(['title' , 'id']);
		$LatestFiles = [];
		foreach ($files as $file){
			$filejson = new Json();
			$filejson->id = $file->id;
			$filejson->title = $file->title;
			$filejson->route = 'file';
			$filejson->link = route('app.file.details' , $file);
			$LatestFiles[] = $filejson;
		}
		return $LatestFiles;
	}

	public function MustDownloads()
	{
		$MustDownloads = [];
		foreach (File::PopulerDownloaded() as $file){
			$filejson = new Json();
			$filejson->id = $file->id;
			$filejson->title = $file->title;
			$filejson->route = 'file';
			$filejson->link = route('app.file.details' , $file);
			$MustDownloads[] = $filejson;
		}
		return $MustDownloads;
	}
}
