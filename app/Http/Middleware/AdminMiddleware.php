<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (Auth::user() && !in_array(Auth::user()->role , [User::Admin ,User::Oprator ,User::SuperAdmin])){
            return redirect()->route("app.home");
        }
        return $next($request);
    }
}
