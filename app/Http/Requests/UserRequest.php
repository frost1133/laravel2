<?php

namespace App\Http\Requests;

use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if(request()->isMethod('PATCH')){
            $user = request()->route("user");
            $rule = [
                "name" => "required|min:5|max:100",
                "email" => ["required","email","min:5","max:100",Rule::unique(User::class , "email")->ignore($user->id)],
                "role" => "required",
                "wallet" => "required",
            ];
            if (request()->input("password")){
                $rule["password"] = "confirmed|min:6|max:200";
            }
            return $rule ;
        };
        return [
            "name" => "required|min:5|max:100",
            "email" => "required|email|min:5|max:100|unique:users,email",
            "password" => "required|confirmed|min:6|max:200",
            "role" => "required",
            "wallet" => "required",
        ];
    }
}
