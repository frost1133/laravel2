<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

/**
 * App\UserDownload
 *
 * @property int $id
 * @property int $user_id
 * @property int $file_id
 * @property string $download_date
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserDownload newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserDownload newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserDownload query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserDownload whereDownloadDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserDownload whereFileId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserDownload whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserDownload whereUserId($value)
 * @mixin \Eloquent
 */
class UserDownload extends Model
{
    public $timestamps = false;

    protected $dateFormat = ["download_date"];

    public static function DaysDownloads()
    {
        $DaysDownloads = DB::table("user_downloads")->select(DB::raw('COUNT(id) as CountDownload , DATE(download_date) as date'))->groupBy(DB::raw('DATE(download_date)'))->get();
        $DaysDownloadsArray = [];
        foreach ( $DaysDownloads as $DayDownload) {
            $DaysDownloadsArray[$DayDownload->date] = $DayDownload->CountDownload;
        }
        return $DaysDownloadsArray;
    }
}
