<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Subscribe
 *
 * @property int $id
 * @property int $user_id
 * @property int $plan_id
 * @property int $download_limit
 * @property int $download_count
 * @property int $payment_amount
 * @property \Illuminate\Support\Carbon $expired_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Plan $plan
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subscribe newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subscribe newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subscribe query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subscribe whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subscribe whereDownloadCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subscribe whereDownloadLimit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subscribe whereExpiredAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subscribe whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subscribe wherePaymentAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subscribe wherePlanId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subscribe whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subscribe whereUserId($value)
 * @mixin \Eloquent
 */
class Subscribe extends Model
{
    protected $guarded = [];

    protected $dates = [
    	"expired_at"
	];

	public function user()
	{
		return $this->belongsTo(User::class);
    }

    public function plan()
	{
		return $this->belongsTo(Plan::class);
    }
}
