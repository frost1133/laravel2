<?php


namespace App\Services\Payment;


use App\Payment;

class Mellat
{
    private $terminalId;
    private $username;
    private $password;
    private $client;
    private $namespace;
    public function __construct()
    {
        $this->terminalId = config("gateways.Mellat.terminalId");
        $this->username = config("gateways.Mellat.userName");
        $this->password = config("gateways.Mellat.password");

        $this->client = new \nusoap_client('https://bpm.shaparak.ir/pgwchannel/services/pgw?wsdl' , true);
        $this->namespace = 'http://interfaces.core.sw.bps.com/';
    }

    public function DoPayment(array $params)
    {
        $args = [
            'terminalId'        =>  $this->terminalId,
            'userName'          =>  $this->username,
            'userPassword'      =>  $this->password,
            'orderId'           =>  $params['orderId'],
            'amount'            =>  $params['amount'],
            'localDate'         =>  date('Ymd'),
            'localTime'         =>  date('Hms'),
            'additionalData'    =>  '',
            'callBackUrl'       =>  route('app.gateway.mellat.verify'),
            'payerId'           =>  0
        ];

        $response = $this->client->call('pbPayRequest' , $args , $this->namespace);

        dd($response);
	    //-- بررسی وجود خطا
	    if ($this->client->fault) {
		    //-- نمایش خطا
		    echo "There was a problem connecting to Bank";
		    exit;
	    } else {
		    $err = $this->client->getError();
		    if ($err) {
			    //-- نمایش خطا
			    echo "Error : " . $err;
			    exit;
		    } else {
			    $statusCode = explode(',', $response);
			    if ($statusCode[0] == "0") {

			    	$newPayment = Payment::create([
			    		'user_id'       =>  $params['user_id'],
					    'method'        =>  Payment::ONLINE,
					    'gateway_name'  =>  'ملت',
					    'res_num'       =>  $params['orderId'],
					    'amount'        =>  $params['amount'],
					    'status'        =>  Payment::INCOMPLETE
				    ]);

			    	if ($newPayment && $newPayment instanceof Payment){
					    $this->RedirectToBank($statusCode[1]);
				    }
			    }
		    }
	    }
    }

    public function VerifyPayment($params)
    {
        if ($params['ResCode'] != '0'){
            return false;
        }

		$args = [
			'terminalId'        =>  $this->terminalId,
			'userName'          =>  $this->username,
			'userPassword'      =>  $this->password,
			'orderId'           =>  $params['orderId'],
			'saleOrderId'       =>  $params['SaleOrderId'],
            'saleReferenceId'   =>  $params['SaleReferenceId']
		];

        $responce = $this->client->call('PbVerifyRequest' , $args , $this->namespace);


        if (!$responce || empty($responce)){
            return false;
        }

        if ($responce['return'] !== '0'){
            return false;
        }

        $paymentItem = Payment::where('ref_id' , '=' , $params['SaleOrderId'])->get();
        $paymentItem->res_num = $params['SaleReferenceId'];
        $paymentItem->status = Payment::COMPLETE;
        $paymentItem->save();

        $setlleArgs = [
            'terminalId'        =>  $this->terminalId,
            'userName'          =>  $this->username,
            'userPassword'      =>  $this->password,
            'orderId'           =>  $params['orderId'],
            'saleOrderId'       =>  $params['SaleOrderId'],
            'saleReferenceId'   =>  $params['SaleReferenceId']
	    ];

        $setlleResponce = $this->client->call( 'bpSettleRequest',$setlleArgs , $this->namespace);

        if ($setlleResponce && $setlleResponce['return'] == '0'){

        }

        return true;
    }


    private function RedirectToBank(string $code){
		?>

	    <form name="myform" action="https://bpm.shaparak.ir/pgwchannel/startpay.mellat" method="POST">
		    <input type="hidden" id="RefId" name="RefId" value="<? echo $code?>">
	    </form>

	    <script type="text/javascript">
		    function formSubmit() { document.forms[0].submit(); }
		    window.onload = formSubmit;
	    </script>

		<?php
    }
}
